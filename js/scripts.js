
(function($, Drupal) {
  // Delete radix dropdown hover
  delete Drupal.behaviors.radix_dropdown;

  Drupal.behaviors.helm_base = {
    attach: function(context, settings) {
      // Open external links in new window
      var reg = new RegExp("/" + window.location.host + "/");
      $("a").each(function() {
        if (!reg.test(this.href)) {
          $(this).attr('target', '_blank');
        }
      });
    }
  };
})(jQuery, Drupal);
